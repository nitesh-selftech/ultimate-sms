<?php

namespace App\Http\Controllers\API\S5;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Models\S5Users;
use Validator;

class S5USersController extends Controller
{
    /* This api is used for testing */
    public function testing(Request $request)
    {
        return response()->json(['status'=>'users testing function']);
    }
    
    /* This api is used for registration */
   public function registration(Request $request)
    {
        if(!empty($request))
        {
            $errors = array();
            $success_msg = '';
            $validator = Validator::make($request->all(), [
            'device_id' => 'required',
            'device_type' => 'required|in:android,ios',
            // 'mobile' => 'required|digits:10',
            'operator' => 'required',
            ]);
            if($validator->fails()) 
            {
                $errors = array('status' => false, 'message' => $validator->errors()->first());
            }
            else 
            {
                try 
                {
                    $checkUser = S5Users::select('*')
                        ->where('mobile', $request->mobile)
                        ->where('device_id', $request->device_id)
                        ->where('device_type', $request->device_type)
                        ->where('operator', $request->operator)
                        ->where('status', '!=', 'delete')
                        ->first();

                    $getUserByMobile = S5Users::select('*')
                        ->where('mobile', $request->mobile)		            
                        ->where('status', '!=', 'delete')
                        ->first();

                    $getUserByDevice = S5Users::select('*')
                        ->where('device_id', $request->device_id)	
                        ->where('status', '!=', 'delete')
                        ->first();

                    if($checkUser)
                    {
                        $user = S5Users::find($checkUser->id);
                        $user_id=$user->id;
                        $user->device_id = $request->device_id;
                        $user->device_type = $request->device_type;
                        $user->mobile = $request->mobile;
                        $user->operator = $request->operator;
                        $user->name = $request->name;
                        $user->email = $request->email;
                        $user->password = $request->password;
                        $user->mobile_alt = $request->mobile_alt;
                        $user->email_verified_at = $request->email_verified_at;
                        $user->operator_alt = $request->operator_alt;
                        $user->sim_no = $request->sim_no;
                        $user->address = $request->address;
                        $user->image = $request->image;                        
                        $user->updated_at = date('Y-m-d H:i:s');
                        $user->save();
                    	$userData=DB::table('s5_users')
                        ->select('id','name','email','mobile','address','device_id')
                        ->where('id',$user_id)
                        ->first();
                    	$success_msg=array('message' => 'Welcome back');
                    	$token = $user->createToken('S5')->accessToken;
                    
                    } else if($getUserByMobile)
                    {
                    	$user = S5Users::find($getUserByMobile->id);
                        $user_id=$user->id;
                        $user->device_id = $request->device_id;
                        $user->device_type = $request->device_type;
                        $user->mobile = $request->mobile;
                        $user->operator = $request->operator;
                        $user->name = $request->name;
                        $user->email = $request->email;
                        $user->password = $request->password;
                        $user->mobile_alt = $request->mobile_alt;
                        $user->email_verified_at = $request->email_verified_at;
                        $user->operator_alt = $request->operator_alt;
                        $user->sim_no = $request->sim_no;
                        $user->address = $request->address;
                        $user->image = $request->image;                        
                        $user->updated_at = date('Y-m-d H:i:s');
                        $user->save();
                        // $user_id=$user->id;
                        $userData=DB::table('s5_users')
                            ->select('id','name','email','mobile','address','device_id')
                            ->where('id',$user_id)
                            ->first();
                    	$success_msg=array('message' => 'User update successfully. ');
                    	$token = $user->createToken('S5')->accessToken;

                    } else if($getUserByDevice)
                    {
                    	$user = S5Users::find($getUserByDevice->id);
                        $user_id=$user->id;
                        $user->device_id = $request->device_id;
                        $user->device_type = $request->device_type;
                        $user->mobile = $request->mobile;
                        $user->operator = $request->operator;
                        $user->name = $request->name;
                        $user->email = $request->email;
                        $user->password = $request->password;
                        $user->mobile_alt = $request->mobile_alt;
                        $user->email_verified_at = $request->email_verified_at;
                        $user->operator_alt = $request->operator_alt;
                        $user->sim_no = $request->sim_no;
                        $user->address = $request->address;
                        $user->image = $request->image;
                        $user->updated_at = date('Y-m-d H:i:s');
                        $user->save();
                        // $user_id=$user->id;
                        $userData=DB::table('s5_users')
                            ->select('id','name','email','mobile','address','device_id')
                            ->where('id',$user_id)
                            ->first();
                    	$success_msg=array('message' => 'User update successfully. ');
                    	$token = $user->createToken('S5')->accessToken;
                    } else {
                    	$user = new S5Users();
                        $user->device_id = $request->device_id;
                        $user->device_type = $request->device_type;
                        $user->mobile = $request->mobile;
                        $user->operator = $request->operator;
                        $user->name = $request->name;
                        $user->email = $request->email;
                        $user->password = $request->password;
                        $user->mobile_alt = $request->mobile_alt;
                        $user->email_verified_at = $request->email_verified_at;
                        $user->operator_alt = $request->operator_alt;
                        $user->sim_no = $request->sim_no;
                        $user->address = $request->address;
                        $user->image = $request->image;
                        $user->created_at = date('Y-m-d H:i:s');
                        $user->save();
                        $user_id=$user->id;
                        $userData=DB::table('s5_users')
                                ->select('id','name','email','mobile','address','device_id')
                                ->where('id',$user_id)
                                ->first();
                        $success_msg=array('message' => 'New User added successfully. ');
                        $token = $user->createToken('S5')->accessToken;
                    }

                }
                catch(Exception $e)
                {
                    $errors = array('message'=> 'Something went wrong, Please try again!');
                }
            }
            if(count($errors) > 0 )
            {
                return $response = json_encode(['status' => false, 'message' => $errors['message']]);
            }
            else
            {
                return $response = json_encode(['status' => true, 'message' => $success_msg['message'], 'userData' => $userData, 'token' => $token]);
            }	
    	}

    }
}
