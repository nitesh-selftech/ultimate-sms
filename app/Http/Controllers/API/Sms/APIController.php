<?php

namespace App\Http\Controllers\API\Sms;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Library\Tool;
use App\Models\Traits\ApiResponser;
use App\Models\SmsUsers;
use App\Models\VerifyUsers;
use DB;
use Auth;
use Illuminate\Http\JsonResponse;
use Validator;
use Illuminate\Support\Str;

class APIController extends Controller
{
    use ApiResponser;

    /**
     * get profile
     *
     * @return JsonResponse
     */
    public function vinodshow(): JsonResponse
    {

        if (config('app.env') == 'demo') {
            return response()->json([
                    'status'  => 'error',
                    'message' => 'Sorry! This option is not available in demo mode',
            ]);
        }

        $data = [
                'name'            => "Chandu",
                'api_token'      => "HBHJNJGG88uhuh",
                'email'          => "cborkar@gmail.com",
                "locale"         => "Nagpur"
        ];

        return $this->success($data);
    }
    
//    public function registration(Request $request)
//    {
//        return response()->json(['data'=>$request->all()]);
//    }
    
    /* This api is used for registration */
    public function registration(Request $request)
    {
        if(!empty($request))
        {
            $errors = array();
            $success_msg = '';
            $no_of_sim_cards = $request->no_of_sim_cards;
            if($no_of_sim_cards=='2'){
                $validator = Validator::make($request->all(), [     
                'device_id' => 'required',
                'device_type' => 'required|in:android,ios',            
                'no_of_sim_cards' => 'required',
                'operator' => 'required',
                'serial_no' => 'required',
                'serial_no_alt' => 'required',         
                ]);
            } else {
                $validator = Validator::make($request->all(), [
                'device_id' => 'required',
                'device_type' => 'required|in:android,ios',            
                'no_of_sim_cards' => 'required',
                'operator' => 'required',
                'serial_no' => 'required',
            ]);
            }
            if($validator->fails()) 
            {
                $errors = array('status' => false, 'message' => $validator->errors()->first());
            }
            else 
            {
                try 
                {
                    $getExistingUser = SmsUsers::select('*')
                        ->where('serial_no', $request->serial_no)
                        ->where('device_id', $request->device_id)
                        ->where('device_type', $request->device_type)
                        ->where('operator', $request->operator)
                        ->where('status', '!=', 'delete')
                        ->first();

                    $getUserBySerialNo = SmsUsers::select('*')
                        ->where('serial_no', $request->serial_no)		            
                        ->where('status', '!=', 'delete')
                        ->first();

                    $getUserByDevice = SmsUsers::select('*')
                        ->where('device_id', $request->device_id)	
                        ->where('status', '!=', 'delete')
                        ->first();

                    if($getExistingUser)
                    {
                        $user = SmsUsers::find($getExistingUser->id);
                        $user_id=$user->id;
                        $user->device_id = $request->device_id;
                        $user->serial_no_alt = $request->serial_no_alt;
                        $user->serial_no = $request->serial_no;
                        $user->device_type = $request->device_type;
                        $user->mobile = $request->mobile;
                        $user->operator = $request->operator;
                        $user->name = $request->name;
                        $user->email = $request->email;
                        $user->password = $request->password;
                        $user->mobile_alt = $request->mobile_alt;
                        $user->email_verified_at = $request->email_verified_at;
                        $user->operator_alt = $request->operator_alt;
                        $user->sim_no = $request->sim_no;
                        $user->address = $request->address;
                        $user->image = $request->image;                        
                        $user->updated_at = date('Y-m-d H:i:s');
                        $token = Str::random(32);
                        $user->api_token = $token;
                        $user->save();
                    	$userData=DB::table('sms_users')
                        ->select('id','name','email','mobile','mobile_alt','address','device_id','serial_no','serial_no_alt')
                        ->where('id',$user_id)
                        ->first();
                    	$success_msg=array('message' => 'Welcome back');
//                    	$token = $user->createToken('S5')->accessToken;
                    } else if($getUserBySerialNo)
                    {
                    	$user = SmsUsers::find($getUserBySerialNo->id);
                        $user_id=$user->id;
                        $user->device_id = $request->device_id;
                        $user->serial_no = $request->serial_no;
                        $user->serial_no_alt = $request->serial_no_alt;
                        $user->device_type = $request->device_type;
                        $user->mobile = $request->mobile;
                        $user->operator = $request->operator;
                        $user->name = $request->name;
                        $user->email = $request->email;
                        $user->password = $request->password;
                        $user->mobile_alt = $request->mobile_alt;
                        $user->email_verified_at = $request->email_verified_at;
                        $user->operator_alt = $request->operator_alt;
                        $user->sim_no = $request->sim_no;
                        $user->address = $request->address;
                        $user->image = $request->image;                        
                        $user->updated_at = date('Y-m-d H:i:s');
                        $token = Str::random(32);
                        $user->api_token = $token;
                        $user->save();
                        // $user_id=$user->id;
                        $userData=DB::table('sms_users')
                            ->select('id','name','email','mobile','mobile_alt','address','device_id','serial_no','serial_no_alt')
                            ->where('id',$user_id)
                            ->first();
                    	$success_msg=array('message' => 'User update successfully. ');
//                    	$token = $user->createToken('S5')->accessToken;

                    } else if($getUserByDevice)
                    {
                    	$user = SmsUsers::find($getUserByDevice->id);
                        $user_id=$user->id;
                        $user->device_id = $request->device_id;
                        $user->serial_no = $request->serial_no;
                        $user->serial_no_alt = $request->serial_no_alt;
                        $user->device_type = $request->device_type;
                        $user->mobile = $request->mobile;
                        $user->operator = $request->operator;
                        $user->name = $request->name;
                        $user->email = $request->email;
                        $user->password = $request->password;
                        $user->mobile_alt = $request->mobile_alt;
                        $user->email_verified_at = $request->email_verified_at;
                        $user->operator_alt = $request->operator_alt;
                        $user->sim_no = $request->sim_no;
                        $user->address = $request->address;
                        $user->image = $request->image;
                        $user->updated_at = date('Y-m-d H:i:s');
                        $token = Str::random(32);
                        $user->api_token = $token;
                        $user->save();
                        // $user_id=$user->id;
                        $userData=DB::table('sms_users')
                            ->select('id','name','email','mobile','mobile_alt','address','device_id','serial_no','serial_no_alt')
                            ->where('id',$user_id)
                            ->first();
                    	$success_msg=array('message' => 'User update successfully. ');
//                    	$token = $user->createToken('S5')->accessToken;
                    } else {
                    	$user = new SmsUsers();
                        $user->device_id = $request->device_id;
                        $user->serial_no = $request->serial_no;
                        $user->serial_no_alt = $request->serial_no_alt;
                        $user->device_type = $request->device_type;
                        $user->mobile = $request->mobile;
                        $user->operator = $request->operator;
                        $user->name = $request->name;
                        $user->email = $request->email;
                        $user->password = $request->password;
                        $user->mobile_alt = $request->mobile_alt;
                        $user->email_verified_at = $request->email_verified_at;
                        $user->operator_alt = $request->operator_alt;
                        $user->sim_no = $request->sim_no;
                        $user->address = $request->address;
                        $user->image = $request->image;
                        $user->created_at = date('Y-m-d H:i:s');
                        $token = Str::random(32);
                        $user->api_token = $token;
                        $user->save();
                        $user_id=$user->id;
                        $userData=DB::table('sms_users')
                                ->select('id','name','email','mobile','mobile_alt','address','device_id','serial_no','serial_no_alt')
                                ->where('id',$user_id)
                                ->first();
                        $success_msg=array('message' => 'New User added successfully. ');
//                        $token = $user->createToken('S5')->accessToken;
                    }

                }
                catch(Exception $e)
                {
                    $errors = array('message'=> 'Something went wrong, Please try again!');
                }
            }
            if(count($errors) > 0 )
            {
                return $response = json_encode(['status' => false, 'message' => $errors['message']]);
            }
            else
            {
                return $response = json_encode(['status' => true, 'message' => $success_msg['message'], 'userData' => $userData, 'token' => $token]);
            }	
        }

    }
    
    /* This api is used for verify numbers */
   public function verifySmsNumbers(Request $request)
    {
        if(!empty($request))
            {
    		$errors = array();
	        $success_msg = '';
	        $validator = Validator::make($request->all(), [
            	'messages_from' => 'required',
            	'body' => 'required',            	
        	]);
        	if($validator->fails()) 
	        {
	            $errors = array('status' => false, 'message' => $validator->errors()->first());
	        }
	        else 
	        {
                    try 
                    {
//                        $user = new SmsUsers();
                        $body = $request->body;
                        $mobile = $request->messages_from;
                        $getSmsUserDetail = json_decode(base64_decode($body),true);
//                        $getSmsUserDetail = json_decode($body, true);
//                        print_r($getSmsUserDetail);
//                        print_r($getSmsUserDetail[0]['body']['device_id']);
                        $data = $getSmsUserDetail[0]['body'];
//                        print_r($data);
//                        die();
                        $verifySmsUser = SmsUsers::select('*')
                                ->where('device_id',$data['device_id'])
                                ->where('serial_no',$data['serial_no'])
                                ->where('id',$data['user_id'])
                                ->first();
//                        dd($verifySmsUser);
                        
                        if($verifySmsUser){
                            $findVerifiedUser = SmsUsers::find($verifySmsUser->id);
//                            dd($findVerifiedUser);
                            $findVerifiedUser->mobile = 9545089878;
                            $findVerifiedUser->body = $data;
//                            dd($findVerifiedUser);
                            $findVerifiedUser->save();
                            $success_msg=array('message' => 'verify number successfully');
                            
                        } else {
                            $errors=array('message' => 'user not available');
                        }
                        
//                        if($checkNumber){
//                            $verifyUser = VerifyNumbers::find($checkNumber->id);
//                            $verify_id=$verifyUser->id;
//                            $userData=DB::table('verify_numbers')
//                            ->select('id','messages_from','body')
//                            ->where('id',$verify_id)
//                            ->first();
//                            $success_msg=array('message' => 'verify number successfully');
////                                $token = $user->createToken('S5')->accessToken;
//                        } 
                    }
                    catch(Exception $e)
	            {
	                $errors = array('message'=> 'Something went wrong, Please try again!');
	            }
	        }
	        if(count($errors) > 0 )
	        {
	            return $response = json_encode(['status' => false, 'message' => $errors['message']]);
	        }
	        else
	        {
	            return $response = json_encode(['status' => true, 'message' => $success_msg['message']]);
	        }	
            }
    }
    
    public function returnStatusOfSync(Request $request)
    {
        if(!empty($request))
            {
    		$errors = array();
	        $success_msg = '';
	        $validator = Validator::make($request->all(), [
            	'device_id' => 'required',
            	'serial_no' => 'required',  
                'user_id'   => 'required',
        	]);
        	if($validator->fails()) 
	        {
	            $errors = array('status' => false, 'message' => $validator->errors()->first());
	        }
	        else 
	        {
                    try 
                    {
                        $SmsUser = SmsUsers::select('id','device_id','serial_no','serial_no_alt','mobile','operator','mobile_alt','operator_alt')
                                ->where('device_id',$request->device_id)
                                ->where('serial_no',$request->serial_no)
                                ->where('id',$request->user_id)
                                ->first();
                        
//                        dd($SmsUser);
                        $findSmsUser = SmsUsers::find($SmsUser->id);
                        $token = Str::random(32);
                        $findSmsUser->api_token = $token;
                        $findSmsUser->save();
                        
                        if($SmsUser){
                            $success_msg=array('message' => 'verify number successfully');
                        } else {
                            $errors = array('message' => 'User not available');
                        }
                    }
                    catch(Exception $e)
	            {
	                $errors = array('message'=> 'Something went wrong, Please try again!');
	            }
	        }
	        if(count($errors) > 0 )
	        {
	            return $response = json_encode(['status' => false, 'message' => $errors['message']]);
	        }
	        else
	        {
	            return $response = json_encode(['status' => true, 'message' => $success_msg['message'],'userData' => $SmsUser,'token' => $token]);
	        }	
            }
    }
	
	public function insertMsgs(Request $request)
    {
            
                     
                    	\DB::table('testsms')->insert([
						 'data'=>'user bin'
						]);    
                        
           
                return $response = json_encode(['status' => true]);
            
            
    }

    

}
